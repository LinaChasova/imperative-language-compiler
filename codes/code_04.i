routine sum(a : integer) is
        var sum is 0
        for i in 1..a loop
                sum := sum + i
        end
end
