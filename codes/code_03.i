type double is real
type arr is array
type rec is record

var product : rec
                var id : integer 
                var price : double
end

var store : arr [10] rec
var total_price : double is 0

for i in 1..10 loop
        product.id := i
        product.price := (i * 4.5 - i) / 0.33
        arr[i] := product
end


for i in 1..10 loop
        total_price := total_price + arr[i].price
end
