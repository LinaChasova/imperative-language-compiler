routine init (arr : array) is
        var length is arr[1]
        var i is 2
        var odd : array [length] integer
        var even : array [length] integer
        var oi is 1
        var ei is 1
        while i <= length loop
                if isOdd(arr[i]) then
                        odd[oi] := arr[i]
                else
                        even[ei] := arr[i]
                end
        end
end

routine isOdd (a : integer) : boolean is
        if a % 2 = 1 then
                return true
        else
                return false
        end
end
