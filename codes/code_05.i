var a : integer is 5
var b : integer is 7
var isEqual : boolean

if a /= b then
        if a < b then
                a := a + 1
        end
else
        isEqual := true
end

var c : real is 4.5
var d : real is 8.334

if d > c then
        c := d / 2
end

var e : real is 3.4
var f : real is 3.4

if f >= e then
        f := e + f
end
                
