import ply.lex as lex
import ply.yacc as yacc

reserved = {
    # DECLARATIONS
    'var' : 'D_VAR',
    'routine' : 'D_ROUTINE',
    'type' : 'D_TYPE',
    'is' : 'D_IS',

    # STATEMENTS
    'else': 'S_ELSE',
    'for' : 'S_FOR',
    'if': 'S_IF',
    'then' : 'S_THEN',
    'while' : 'S_WHILE',
    'return': 'S_RETURN',
    'loop' : 'S_LOOP',
    'in' : 'S_IN',
    'end' : 'S_END',
    'reverse' : 'S_REVERSE',

    # EXPRESSION
    'true' : 'E_TRUE',
    'false' : 'E_FALSE',

    # DEFAULT CLASSES
    'integer' : 'class_INT',
    'real' : 'class_REAL',
    'boolean' : 'class_BOOL',

    # COLLECTIONS
    'record': 'collection_RECORD',
    'array': 'collection_ARRAY',

    # OPERATORS
    'and' : 'O_AND',
    'or' : 'O_OR',
    'xor' : 'O_XOR',
    'not' : 'O_NOT'
}

tokens = [
    'ID', 'INT', 'REAL', 'ASSIGN', 'PLUS', 'MINUS', 'MULT', 'DIV', 'MOD', 'GREATER', 'GREATER_EQ', 'LESS',
    'LESS_EQ', 'EQUAL', 'NOT_EQUAL', 'LPAREN', 'RPAREN', 'RBRACKET', 'LBRACKET', 'DOT', 'COMMA', 'COLON',
    'SEMICOLON', 'RANGE'
] + list(reserved.values())

t_PLUS = r'\+'
t_MINUS = r'-'
t_MULT = r'\*'
t_DIV = r'/'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_DOT = r'\.'
t_COMMA = r','
t_COLON = r':'
t_SEMICOLON = r';'
t_ASSIGN = r':='
t_RANGE = r'\.\.'
t_MOD = r'%'
t_GREATER = r'>'
t_GREATER_EQ = r'>='
t_LESS = r'<'
t_LESS_EQ = r'<='
t_EQUAL = r'='
t_NOT_EQUAL = r'/='


def t_ID(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value, 'ID')  # Check for reserved words
    return t


def t_REAL(t):
    r'[0-9]*\.[0-9]+'
    t.value = float(t.value)
    return t


def t_INT(t):
    r'\d+'
    t.value = int(t.value)
    return t

# Ignored characters
t_ignore = " \t\r"


def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


lexer = lex.lex(debug=0)


def tokenize():
    # Give the lexer some input
    file = open('in.txt', 'r', encoding='utf8')
    data = file.read()
    lexer.input(data)
    file.close()

    w = lexer.token()
    while w is not None:
        print(w)
        w = lexer.token()

tokenize()